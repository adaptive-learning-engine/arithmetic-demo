/* eslint-disable no-console */
import dotenv from 'dotenv';

import { App } from './App';
import { Logger } from './Logger';

dotenv.config();

const { HOST, DEVELOPMENT, LAPTIME, ACTIVITY_SUBSET, ADLETE_USERNAME, ADLETE_PASSWORD } = process.env;

if (HOST !== undefined) {
  // create a new app object
  const app = new App(
    HOST,
    DEVELOPMENT ? DEVELOPMENT === 'true' : false,
    +LAPTIME,
    ACTIVITY_SUBSET ? JSON.parse(ACTIVITY_SUBSET) : ACTIVITY_SUBSET,
    ADLETE_USERNAME,
    ADLETE_PASSWORD
  );

  try {
    // initialize the game and establish a connection to the service
    if (await app.initialsGame()) {
      // if a connection to the service could be established start the game
      await app.runMainGameLoop();
    }
  } catch (error) {
    // end the game if something went wrong
    await app.endGame(error);
  } finally {
    // end game if the user existed the game on purpose
    await app.endGame();
  }
} else {
  Logger.error('Error: Missing .env-File, find further instruction in the ReadMe.md and .env.example files.');
}
