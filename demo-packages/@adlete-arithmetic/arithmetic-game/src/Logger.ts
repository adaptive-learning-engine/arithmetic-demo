/* eslint-disable no-console */

export const Logger = {
  log(message: unknown, devFlag = true): void {
    if (devFlag) console.log(message);
  },

  info(message?: unknown, devFlag = true): void {
    if (devFlag) console.info(message);
  },

  warn(message?: unknown, devFlag = true): void {
    if (devFlag) console.warn(message);
  },

  error(message?: unknown, devFlag = true): void {
    if (devFlag) console.error(message);
  },
};
