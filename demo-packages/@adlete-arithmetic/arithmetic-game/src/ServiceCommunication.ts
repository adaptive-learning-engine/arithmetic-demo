import { AdleteServiceConnection } from '@adlete/client-ts/AdleteServiceConnection';
import { ITokenResponse } from '@adlete/client-ts/Types';
import { IActivityRecommendation } from '@adlete/engine-blocks/recommendation/IActivityRecommender';

import { IArithmeticActivityWithDifficulty } from './ArithmeticExerciseGenerator';
import { Logger } from './Logger';

// #region ServiceRoutines
export async function checkStatus(adleteService: AdleteServiceConnection): Promise<void> {
  const responseCheckStatus = await adleteService.checkStatus();
  if (responseCheckStatus.statusCode !== 200) {
    throw new Error('Unable to connect to adlete service!');
  }
}

export async function loginUser(adleteService: AdleteServiceConnection, username: string, password: string): Promise<ITokenResponse> {
  return adleteService.adleteLoginUser(username, password)
}

export async function refreshLogin(adleteService: AdleteServiceConnection, refreshToken:string): Promise<ITokenResponse> {
  return adleteService.adleteRefreshLogin(refreshToken)
}
// #endregion

// #region LearnerRoutines
export async function createNewLearnerAndLogin(adleteService: AdleteServiceConnection, initialScalarBeliefSetId: string): Promise<string> {
  const response = await adleteService.createNewLearnerAndLogin(initialScalarBeliefSetId);
  if (response.statusInfo.statusCode !== 200) {
    throw new Error(`Unable to create Learner! ${response.statusInfo.statusDescription}`);
  }
  return response.learnerId;
}

export async function loginLearner(adleteService: AdleteServiceConnection, learnerId: string): Promise<void> {
  const response = await adleteService.login(learnerId);
  if (response.statusInfo.statusCode !== 200) {
    throw new Error(`Unable to login with learner ID ${learnerId}!`);
  }
}
// #endregion

// #region ActivityRoutines
export async function fetchRecommendation(
  adleteService: AdleteServiceConnection,
  activitySubset: string[],
  isDevMode = false
): Promise<IActivityRecommendation<number>> {
  const response = await adleteService.fetchNextActivityRecommendation(activitySubset);
  if (response.statusInfo.statusCode !== 200) {
    Logger.log(`\x1b[31m${response.statusInfo.statusDescription}\x1b[0m`, isDevMode);
    throw new Error(`Error fetching recommendation: ${response.statusInfo.statusDescription}`);
  }

  return response.recommendation;
}

export async function submitResults(
  adleteService: AdleteServiceConnection,
  activityWithDiff: IArithmeticActivityWithDifficulty,
  correctness: number
): Promise<void> {
  const { activityName, generatedDifficulty } = activityWithDiff;
  const response = await adleteService.submitActivityResult(activityName, correctness, generatedDifficulty);
  if (response.statusCode !== 200) {
    Logger.log(`\x1b[31m ${response.statusDescription}\x1b[0m`);
    throw new Error(`Error updating learner model: ${response.statusDescription}`);
  }
}
// #endregion

// #region Session
export async function startSession(adleteService: AdleteServiceConnection): Promise<void> {
  await adleteService.startSession();
}

export async function activeSession(adleteService: AdleteServiceConnection): Promise<boolean> {
  const response = await adleteService.activeSession();

  return response.activeSession != null;
}

export async function stopSession(adleteService: AdleteServiceConnection): Promise<void> {
  await adleteService.stopSession();
}
// #endregion
