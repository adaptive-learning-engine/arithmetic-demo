export class Timer {
  time: number;

  finishTime: number;

  t: NodeJS.Timeout;

  timerIsOn: boolean;

  constructor(finishTime: number) {
    this.time = 0;
    this.finishTime = finishTime;
    this.startCount();
  }

  timedCount(): void {
    this.time += 1;
    this.t = setTimeout(() => this.timedCount(), 1000);
  }

  startCount(): void {
    if (this.timerIsOn) {
      return;
    }
    this.timerIsOn = true;
    this.timedCount();
  }

  isTimeUp(): boolean {
    if (this.timerIsOn || this.time < this.finishTime) {
      return false;
    }

    this.stopCount();

    return true;
  }

  stopCount(): void {
    clearTimeout(this.t);
    this.timerIsOn = false;
  }
}
