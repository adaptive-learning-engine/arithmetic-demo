import { Logger } from './Logger';

export interface IArithmeticActivity {
  number1: number;
  number2: number;
  operator: string;
  result: number;
}

export interface IArithmeticActivityWithDifficulty {
  activity: IArithmeticActivity;
  activityName: string;
  recommendedDifficulty: number;
  generatedDifficulty: number;
}

export class ArithmeticExerciseGenerator {
  private generateCalcNumbers(recommendedDifficulty: number): { number1: number; number2: number } {
    // We use the function: f(x) = (1/2*x) * log(x+1)*722 + 1 to generate the first number in a range between 1 and 255.
    // To visualize this function see the following link: https://www.wolframalpha.com/input?i=plot+%281%2F2*x%29+*+log%28x%2B1%29*722+%2B+1%2C+x%3D0+to+1
    // We use this function with the recommendedDifficulty value in the range from 0 to 1 and adding a small value.

    let x = recommendedDifficulty + Math.trunc(Math.random() * 10) * 0.01;
    const number1 = Math.round(0.5 * x * Math.log(x + 1) * 722 + 1);

    // We use the function: f(x) = (1/3*x) * log(x+1)*110 + 1 to generate the first number in a range between 1 and 25.
    // To visualize this function see the following link: https://www.wolframalpha.com/input?i=plot+%281%2F3*x%29+*+log%28x%2B1%29*110+%2B+1%2C+x%3D0+to+1
    // We use this function with the recommendedDifficulty value in the range from 0 to 1.

    x = recommendedDifficulty + Math.trunc(Math.random() * 10) * 0.01;
    const number2 = Math.round((1 / 3) * x * Math.log(x + 1) * 110 + 1);

    return { number1, number2 };
  }

  generateActivity(recommendedActivityName: string, recommendedDifficulty: number): IArithmeticActivityWithDifficulty {
    Logger.log(`\x1b[35m
      New estimated difficulty level for ${recommendedActivityName.replace(/([A-Z])/g, ' $1').toLowerCase()}: ${recommendedDifficulty}
      \x1b[0m`);

    // generate Activity
    // generate Numbers
    const { number1, number2 } = this.generateCalcNumbers(recommendedDifficulty);
    // activity operator
    let operator = '';
    // activity result
    let result: number;

    switch (recommendedActivityName) {
      case 'activityAddition':
        operator = '+';
        result = Math.trunc(number1 + number2);
        break;
      case 'activitySubtraction':
        operator = '-';
        result = Math.trunc(number1 - number2);
        break;
      case 'activityMultiplication':
        operator = '*';
        result = Math.trunc(number1 * number2);
        break;
      case 'activityDivision':
        operator = '/';
        result = Math.trunc(number1 / number2);
        break;
      default:
        result = 0;
        break;
    }
    return {
      activity: {
        number1,
        number2,
        operator,
        result,
      },
      recommendedDifficulty,
      generatedDifficulty: recommendedDifficulty,
      activityName: recommendedActivityName,
    };
  }
}
