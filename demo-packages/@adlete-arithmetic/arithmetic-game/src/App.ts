/* eslint-disable no-await-in-loop */
import { createInterface, Interface } from 'readline';

import { CronJob } from 'cron';

import { AdleteServiceConnection } from '@adlete/client-ts/AdleteServiceConnection';

import { ArithmeticExerciseGenerator, IArithmeticActivity } from './ArithmeticExerciseGenerator';
import { Logger } from './Logger';
import * as ServiceCommunication from './ServiceCommunication';
import { Timer } from './Timer';


export class App {
  private host: string;

  private username: string;

  private password: string;

  private token: string;
  private refreshToken: string

  private development: boolean;

  private lapTime: number;

  private adleteService: AdleteServiceConnection;

  private readLine: Interface;

  private rightAnswersCount = 0;

  private questionsCount = 0;

  private activitySubset: string[];
  private cronJob: CronJob

  constructor(host: string, development = false, lapTime = 60, activitySubset: string[], username?: string, password?: string) {
    this.host = host;
    this.username = username;
    this.password = password;
    this.development = development;
    this.lapTime = lapTime;
    this.activitySubset = activitySubset;
    this.adleteService = new AdleteServiceConnection(this.host, 'arithmetic-game');

    this.readLine = createInterface({
      input: process.stdin,
      output: process.stdout,
    });
  }

  /**
   * Before the game can start we have to establish a connection to the service.
   * Additionally we have to login the learner or create for him/her a new learner account.
   *
   * @returns AdleteServiceConnection which holds the connection to the service.
   */
  public async initialsGame(message?: string): Promise<boolean> {
    if(message) {
      console.log(message)
    }
    /** *
     * 1. Step: Check if the Service available and your are authorized
     */
    await ServiceCommunication.checkStatus(this.adleteService);

    /**
     * 2. Step Login and get tokens and refresh Cron job otherwise restart
     */
    if(!this.username) {
      this.username = await this.getUsername()
    }

    if(!this.password) {
      this.password = await this.getPassword()
    }

    try {
      const tokens = await ServiceCommunication.loginUser(this.adleteService, this.username, this.password)
      this.token = tokens.accessToken;
      this.refreshToken = tokens.refreshToken;
      this.adleteService = new AdleteServiceConnection(this.host, 'arithmetic-game', this.token)
      
       this.cronJob = CronJob.from({
        cronTime: '* 29 * * * *',
        onTick: () => {
          ServiceCommunication.refreshLogin(this.adleteService, this.refreshToken)
          .then(response => {
            this.token = response.accessToken;
            this.adleteService = new AdleteServiceConnection(this.host, 'arithmetic-game', this.token)
          })
        }, start: true,
      })
      /** *
       * 2. Step: The learner must enter if
       *    (A) he/she has already an account,
       *    (B) he/she would like to create a new one.
       *    (C) he/she would like to exit the application.
       */
      const input = await this.askQuestion('Please enter your learner ID, enter NEW to create a new learner or EXIT to exit the app: ');
      switch (input.toUpperCase()) {
        //
        /**
         * 2.A. Step: The learner would like login with given input
         */
        default:
          await ServiceCommunication.loginLearner(this.adleteService, input);
          break;
  
        /**
         * 2.B. Step: The learner would like to create a new learner account
         */
        case 'NEW':
          await this.createNewLearnerAndLogin(this.adleteService);
          break;
  
        // End application
        case 'EXIT':
          Logger.log('Bye...');
          this.cronJob.stop();
          this.readLine.close();
          return false;
      }
  
      /**
       * 3. Step: A learner needs an active session we check if he/she has one otherwise we create one
       */
      const learnerHasAnActiveSession = await ServiceCommunication.activeSession(this.adleteService);
      if (!learnerHasAnActiveSession) {
        await ServiceCommunication.startSession(this.adleteService);
      }
      return true;
    } catch (err) {
      this.username = null;
      this.password = null;
      await this.initialsGame('Username or Password are incorrect. Please try again.')
    }
  }

  /**
   * For the process of creating a new learner it is necessary to get additional information
   * about the learner. The function processes the following steps:
   *  (1. Step) Ask the learner about his/her general arithmetic competence
   *  (2. Step) Start learner creating process
   *  (3. Step) Log the learner in.
   * @param adleteService
   * @returns
   */
  protected async createNewLearnerAndLogin(adleteService: AdleteServiceConnection): Promise<void> {
    // ***********************1. Step******************************
    // fetch Service configuration to get all initial Scalar Belief Sets
    // the learner should decided on its own on which competence level he/she is?
    const responseServiceConfiguration = await adleteService.fetchServiceConfiguration();
    if (responseServiceConfiguration.statusInfo.statusCode !== 200) {
      throw new Error('Unable to fetch service configuration');
    }
    const initialScalarBeliefs: string[] = Object.keys(responseServiceConfiguration.serviceConfiguration.initialScalarBeliefs);
    const initialScalarBeliefQuestion: string = '[1]-'.concat(
      initialScalarBeliefs.reduce((previousValue: string, key: string, i: number) => previousValue.concat(` [${i + 1}]-${key}`))
    );

    let selectedInitialScalarBeliefSetIdIndex;
    do {
      selectedInitialScalarBeliefSetIdIndex = Math.trunc(
        parseFloat(await this.askQuestion(`Are you ${initialScalarBeliefQuestion} please select the number:`))
      );
    } while (
      selectedInitialScalarBeliefSetIdIndex < 1 ||
      selectedInitialScalarBeliefSetIdIndex > initialScalarBeliefs.length ||
      Number.isNaN(selectedInitialScalarBeliefSetIdIndex)
    );

    // ***********************2. Step & 3. Step******************************
    // the learner will be created with the selected initial scalar belief set
    const selectedInitialScalarBeliefSetId = initialScalarBeliefs[selectedInitialScalarBeliefSetIdIndex - 1];
    const learnerId = await ServiceCommunication.createNewLearnerAndLogin(adleteService, selectedInitialScalarBeliefSetId);
    Logger.log(`\x1b[32mLearner with ID:${learnerId}) created,\nand initial competence model ${selectedInitialScalarBeliefSetId}\x1b[0m`);
  }

  /**
   * A routine to start a command line dialogue with the learner.
   * @param question
   * @returns returns the given answer by the user
   */
  protected async askQuestion(question: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      try {
        this.readLine.question(question, (answer) => {
          resolve(answer);
        });
      } catch (e) {
        reject(e);
      }
    });
  }

  protected async getUsername(): Promise<string> {
    return this.askQuestion("Username:")
  }

    protected async getPassword(): Promise<string> {
      return this.askQuestion("Password: ")
}

  /**
   * Within the game the learner must solve dynamically generated arithmetic exercises.
   * Their generation process is based on the recommendation of the service.
   * Each game round has a time limit which is defined in the .env-File.
   * @param adleteService
   */
  public async runMainGameLoop(): Promise<void> {
    Logger.log(`\x1b[4mSolve the arithmetic activities. For division, round to whole number!\x1b[0m`);

    const arithmetics = new ArithmeticExerciseGenerator();
    const timer = new Timer(this.lapTime);
    let input;

    do {
      // ask for a new recommendation
      const recommendation = await ServiceCommunication.fetchRecommendation(this.adleteService, this.activitySubset, this.development);
      // generate a new activity
      const fullAct = arithmetics.generateActivity(recommendation.activityName, recommendation.difficulty);

      // present
      input = await this.askQuestion(`${fullAct.activity.number1} ${fullAct.activity.operator} ${fullAct.activity.number2} = `);
      // exit game if the input was exit
      if (input === 'EXIT') break;

      // analyze
      const correctness = this.analyzeAnswer(fullAct.activity, input);
      this.rightAnswersCount += correctness;
      this.questionsCount += 1;

      // check timer and if round is up
      if (timer.isTimeUp()) {
        Logger.log('\x1b[36m Unfortunately your time is over...\x1b[0m');
        Logger.log(
          `\x1b[36m you answered ${this.rightAnswersCount} out of ${this.questionsCount} questions correctly in ${this.lapTime} seconds\x1b[0m`
        );
        this.rightAnswersCount = 0;
        this.questionsCount = 0;

        // restart timer for next round
        timer.startCount();
        Logger.log('Starting next round!');
      }

      // updating learner model
      await ServiceCommunication.submitResults(this.adleteService, fullAct, correctness);

      // end game
    } while (input.toUpperCase() !== 'EXIT');
  }

  public async endGame(errorMessage?: string): Promise<void> {
    if (errorMessage !== undefined) {
      Logger.log(errorMessage);
    }
    // closing game
    await ServiceCommunication.stopSession(this.adleteService);
    this.readLine.close();
  }

  protected analyzeAnswer(activity: IArithmeticActivity, answer: string): number {
    const isAnswerCorrect = +answer === activity.result;
    if (isAnswerCorrect) {
      Logger.log(`\x1b[32m Given answer was correct! \x1b[0m `);
    } else {
      Logger.log(`\x1b[31m Given answer was false! \x1b[0m `);
    }

    return isAnswerCorrect ? 1 : 0;
  }
}
