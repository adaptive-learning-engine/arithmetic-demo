# @adlete-arithmetic/arithmetic-game

a prototype for a client using the adlete framework

The player has x seconds time to answer as many questions as possible. The better he scores, the closer to 1 is the number he gets. The adaption options are what kind of basic arithmetic is the algorithm going to choose

score = 0.4

level 1: 0 [+]
level 2: 0.2 [-]
level 3: 0.3 [*]
level 4: 0.4 [/]

Further the training can be adletepted with a changing Interval in which numbers appear;

level 1: 1 - 10 0 [10]
level 2: 10 - 50 0.2 [50]
level 3: 50 - 100 0.4 [100]
level 4: 100 - 1000 0.55 [1000]

--> to connect these options, the difficulty can be calculated by adding up combinations of the 2 adleteption options.

0 [+] - 0 [10] = 0
0 [+] - 0.2 [50] = 0.2
0.2 [-] - 0 [10] = 0.2
0.3 [*] - 0 [10] = 0.3
0 [+] - 0.4 [100] = 0.4
0.2 [-] - 0.2 [50] = 0.4
0.4 [/] - 0 [10] = 0.4
0.3 [*] - 0.2 [50] = 0.5
0 [+] - 0.55 [1000] = 0.55
0.2 [-] - 0.4 [100] = 0.6
0.4 [/] - 0.2 [50] = 0.6
0.3 [*] - 0.4 [100] = 0.7
0.2 [-] - 0.55 [1000] = 0.75
0.4 [/] - 0.4 [100] = 0.8
0.3 [*] - 0.55 [1000] = 0.85
0.4 [/] - 0.55 [1000] = 0.95

Later on further adleteption can be made with finding out at what arithmetic operators the user performed poorly in order to give him easier and/or more options in this area so that the user can keep his learning flow.

The Workflow is as follows:

1. User Login
2. new session gets started
3. Players score gets fetched
4. User plays a single activity which
5. gets submitted to the server
6. then back to number 3) if the time is not over yet else we jump to number
7. stop the session

## Development

### Dependencies

- [node](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/)

### Install package dependencies

```bash
yarn install
```

### Building

This package uses [Gulp](https://gulpjs.com/) and [Typescript](http://typescriptlang.org/).

```bash
# clean and build
yarn build

# watch and build
yarn watch:build

# clean only
yarn clean
```

### IDE

#### Editorconfig

This package uses [Editorconfig](https://editorconfig.org/).

#### Linting

This package uses a prettier configuration to set a style format for all source code files, we ship this config in the package @adlete/dev-config.

### Version Control

Use the [bluejava git commit message guide](https://github.com/bluejava/git-commit-guide).
