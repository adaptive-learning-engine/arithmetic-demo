import path from 'path';

import dotenv from 'dotenv';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// load .env-File into environment variables
dotenv.config();

const { VISUALIZER_PORT } = process.env;

const port = VISUALIZER_PORT || 3000;

const aliasPackages = ['@fki/editor-core', '@fki/plugin-system', '@adlete/engine-blocks', '@adlete/client-ts', '@adlete/visualizer-core'];

function addAliasPackages(aliases, aliasPackages) {
  aliasPackages.forEach((pkg) => {
    aliases[pkg] = path.join(path.dirname(require.resolve(`${pkg}/package.json`)), 'src');
  });
}

const alias = {};
addAliasPackages(alias, aliasPackages);

// https://vitejs.dev/config/
export default defineConfig({
  // root: path.join(__dirname, 'public'),
  plugins: [react()],
  build: {
    outDir: "build"
  },
  assetsInclude: ["**/*/*.node"],
  resolve: {
    // resolve these modules directly from typescript source code (not builds!)
    alias: alias,
    extensions: ['.js', 'd.ts', '.ts', '.tsx', '.node'],
  },
  server: {
    port: port,
  },
});
