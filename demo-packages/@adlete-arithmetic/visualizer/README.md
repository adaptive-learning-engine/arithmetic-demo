# @adlete/visualizer

The visualizer residing in the @adaptive-difficulty package consists of different components:

#### visualizer-core

- the visualizer's element, containing the logic
- implements all React components

#### visualizer-web

- is based on the visualizer-core
- contains the webpack build pipeline and inherits the logic for the web version

#### visualizer-electron

- is likewise based on the visualizer-core
- describes the electron container for the visualizer's stand-alone application

The components are further described in [the development structure](#structure)

## visualizer dependencies

- [Gulp](https://gulpjs.com/)
- [Typescript](http://typescriptlang.org/)

- [node](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/)

- package dependencies can easily be installed:

```bash
yarn install
```

## Building the visualizer

After adding dependencies, the visualizer needs to be built.

Either rooting from the same directory:

```bash
# clean and build
yarn build

# watch and build
yarn watch:build

# clean only
yarn clean
```

Or, if building from the core directory cochlea-adlete:

```bash
yarn build:visualizer
```

## Starting the visualizer

The visualizer needs to load a `visualizerExtension` class to function.

Currently you need to provide the package name of this extension as a command-line argument when building from that directory, e.g.:

```bash
yarn start
```

When starting from cochlea-adlete:

```bash
yarn start:visualizer
```

## visualizer development

### Structure

<a name="structure"></a>

#### visualizer-electron

[Electron](https://electronjs.org/)-based visualizer application for learning applications and games that use `@adaptive-difficulty/framework`. Provides facilities for simulating agents and analysing user data.
Electron applications are split into `main` and `renderer`. In this package, all the interesing stuff happens in the `renderer`.

#### visualizer-web

The UI of the visualizer uses [React](https://reactjs.org/) and [Material-UI](https://material-ui.com/).

#### visualizer-core

Source Hierarchy
The visualizer UI is split into multiple React Components.
Those are subdivided as follows:

![What is this](visualizer_doc.png)

1.  App
2.  ConfigurationsTab, SimulationResultsTab, UserAnalysisTab
3.  LoadResultsView, NewSimulationView, GraphView, GridView
4.  SelectionSearchContainer, GraphContainer, GridContainer
5.  GraphSelectionWidget, GraphWidget, SearchFieldWidget, SelectionSliderWidget
6.  BeliefBarChart, BeliefLinePlot, Drawer, ObservationPlot, Plots, ScalarBeliefLinePlot, GraphControls, GraphMiniMap, SliderComponents
7.  results, activity-transform, AppEnum, SaveJSONSchema

The visualizer loads the simulation results and checks them against a template, which is located in `utility/saveJSONSchema/template.json`.
`save-load-routines.ts` on the other side retracts the template from `build/utility/saveJSONSchema/template.json`, therefore, the template has to be placed there as well.

In future versions it would be handy to enhance the automated template generation when saving the simulation results. Currently, the template generation is incomplete and requires manual correction.

### IDE

#### Editorconfig

This package uses [Editorconfig](https://editorconfig.org/).

#### Linting

This package uses a prettier configuration to set a style format for all source code files, we ship this config in the package @adlete/dev-config.

#### Version Control

Use the [bluejava git commit message guide](https://github.com/bluejava/git-commit-guide).

### Writing a visualizerExtension

Copy the `IvisualizerExtension` interface to a new package and default-export a class, which implements this interface.

Copying is currently necessary, because we don't want a circular dependency between the `visualizer` and the extension.
