/* eslint-disable no-debugger */
import React from 'react';
import { createRoot } from 'react-dom/client';

import { App } from './app/App';

const container = document.getElementById('root');
const root = createRoot(container);

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);

// adding quick debugger functionality
declare global {
  interface Window {
    debuggerIn: (seconds: number) => void;
  }
}

window.debuggerIn = (seconds: number) => {
  setTimeout(() => {
    debugger;
  }, seconds * 1000);
};
