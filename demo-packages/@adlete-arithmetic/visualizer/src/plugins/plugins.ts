import type { IWithLayoutManager } from '@adlete/visualizer-core/plugin-layout/LayoutManagerPlugin';
import type { IWithLearnerAnalysis } from '@adlete/visualizer-core/plugin-learner-analysis/LearnerAnalysisPlugin';
import type { IWithModelBuilder } from '@adlete/visualizer-core/plugin-model-builder/ModelBuilderPlugin';
import type { IWithSimulation } from '@adlete/visualizer-core/plugin-simulation/SimulationPlugin';
import type { IWithThemeManager } from '@adlete/visualizer-core/plugin-theme/ThemeManagerPlugin';

import type { IWithAppCore } from '../app/AppCore';

export interface IEditorPlugins
  extends IWithAppCore,
    IWithLayoutManager,
    IWithThemeManager,
    IWithModelBuilder,
    IWithLearnerAnalysis,
    IWithSimulation {}
