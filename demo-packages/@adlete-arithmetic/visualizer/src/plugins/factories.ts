import type { IPluginFactory, IPluginMeta } from '@fki/plugin-system/specs';

import { LayoutManagerMeta, LayoutManagerPlugin } from '@adlete/visualizer-core/plugin-layout/LayoutManagerPlugin';
import { LearnerAnalysisMeta, LearnerAnalysisPlugin } from '@adlete/visualizer-core/plugin-learner-analysis/LearnerAnalysisPlugin';
import { ModelBuilderMeta, ModelBuilderPlugin } from '@adlete/visualizer-core/plugin-model-builder/ModelBuilderPlugin';
import { SimulationMeta, SimulationPlugin } from '@adlete/visualizer-core/plugin-simulation/SimulationPlugin';
import { ThemeManagerMeta, ThemeManagerPlugin } from '@adlete/visualizer-core/plugin-theme/ThemeManagerPlugin';

import { BUILTIN_LAYOUTS } from './layout/builtinLayouts';
import { IEditorPlugins } from './plugins';

// helper function to create non-async factories
function createInstantFactory<K extends keyof IEditorPlugins>(
  meta: IPluginMeta<IEditorPlugins, K>,
  create: () => IEditorPlugins[K]
): IPluginFactory<IEditorPlugins, K> {
  return {
    meta,
    create: () => Promise.resolve(create()),
  };
}

export const FACTORIES: IPluginFactory<IEditorPlugins, keyof IEditorPlugins>[] = [
  createInstantFactory(LayoutManagerMeta, () => new LayoutManagerPlugin(BUILTIN_LAYOUTS)),
  createInstantFactory(ThemeManagerMeta, () => new ThemeManagerPlugin()),
  createInstantFactory(ModelBuilderMeta, () => new ModelBuilderPlugin()),
  createInstantFactory(LearnerAnalysisMeta, () => new LearnerAnalysisPlugin()),
  createInstantFactory(SimulationMeta, () => new SimulationPlugin()),
];
