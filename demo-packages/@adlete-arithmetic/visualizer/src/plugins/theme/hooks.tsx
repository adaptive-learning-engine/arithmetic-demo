import { useStatefulEvent } from '@fki/editor-core/events/hooks';
import { Theme, ThemeProvider } from '@mui/material';
import React, { FunctionComponent } from 'react';

import { usePlugin } from '../../app/hooks';

export interface ICustomThemeProviderProps {
  children: React.ReactNode;
}

export function useCustomTheme(): Theme {
  const themeMan = usePlugin('themeManager');
  return useStatefulEvent(themeMan.events, 'themeChanged');
}

export const CustomThemeProvider: FunctionComponent<ICustomThemeProviderProps> = ({ children }: ICustomThemeProviderProps) => {
  const theme = useCustomTheme();
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
