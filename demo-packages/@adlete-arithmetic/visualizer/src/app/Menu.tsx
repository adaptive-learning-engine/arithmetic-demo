import { AddPanelMenu } from '@fki/editor-core/layout/AddPanelMenu';
import { LayoutChooser } from '@fki/editor-core/layout/LayoutChooser';
import { ToggleDarkLightButton } from '@fki/editor-core/theme/ToggleDarkLightButton';
import MenuIcon from '@mui/icons-material/Menu';
import { AppBar, Box, IconButton, Menu, MenuItem, Toolbar } from '@mui/material';
import { CommonProps } from '@mui/material/OverridableComponent';
import { styled, Theme } from '@mui/material/styles';
import React, { FunctionComponent } from 'react';

import { usePlugin } from './hooks';

function styleFn({ theme }: { theme: Theme }) {
  return `
  & .layout-chooser {
    margin-right: ${theme.spacing(2)};
  }

  & .title {
    flex-grow: 1;
  }`;
}

export interface IMenuProps extends CommonProps {
  menuActions: JSX.Element
}

export const AppMenu: FunctionComponent<IMenuProps> = styled(({ className, menuActions }: IMenuProps) => {
  const layoutMan = usePlugin('layoutManager');
  const themeMan = usePlugin('themeManager');

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar className={className} position="relative" color="transparent">
      <Toolbar variant="dense">
        <IconButton
          edge="start"
          color="inherit"
          aria-label="menu"
          aria-controls={open ? 'basic-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={handleClick}
        >
          <MenuIcon />
        </IconButton>
        {/* <Typography className="title">Arithmetics Visualizer</Typography> */}
        <AddPanelMenu layoutManager={layoutMan} />
        {menuActions}
        <Box style={{ position: 'absolute', right: '10px' }} className="title">
          <ToggleDarkLightButton themeManager={themeMan} />
        </Box>
      
      </Toolbar>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem>
          {' '}
          <LayoutChooser layoutManager={layoutMan} />
        </MenuItem>
      </Menu>
    </AppBar>
  );
})(styleFn);
