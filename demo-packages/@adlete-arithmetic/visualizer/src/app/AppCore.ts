import { GraphPluginManager } from '@fki/plugin-system';
import { PluginFactoryRegistry } from '@fki/plugin-system/PluginFactoryRegistry';
import type { IPlugin, IPluginMeta } from '@fki/plugin-system/specs';

import { FACTORIES } from '../plugins/factories';
import { IEditorPlugins } from '../plugins/plugins';

export interface IWithAppCore {
  appCore: AppCore;
}

export const AppCoreMeta: IPluginMeta<IEditorPlugins, 'appCore'> = {
  id: 'appCore',
};

export class AppCore implements IPlugin<IEditorPlugins> {
  public meta = AppCoreMeta;

  public pluginFactories: PluginFactoryRegistry<IEditorPlugins>;

  public plugins: GraphPluginManager<IEditorPlugins>;

  protected pluginSequence: (keyof IEditorPlugins)[][];

  public async start(): Promise<void> {
    this.pluginFactories = new PluginFactoryRegistry<IEditorPlugins>();

    FACTORIES.forEach((factory) => {
      this.pluginFactories.add(factory.meta.id, factory);
    });

    const selfFactory = {
      meta: AppCoreMeta,
      create: () => Promise.resolve(this),
    };
    this.pluginFactories.add(AppCoreMeta.id, selfFactory);

    this.plugins = new GraphPluginManager<IEditorPlugins>(this.pluginFactories);

    this.pluginSequence = this.plugins.createSequence();
    // eslint-disable-next-line no-console
    console.log('Initializing plugins in sequence: ', this.pluginSequence);
    await this.plugins.executeCreationPhase(this.pluginSequence);
    await this.plugins.executeInitializationPhase(this.pluginSequence);
  }

  public async shutdown(): Promise<void> {
    await this.plugins.executeTerminationPhase(this.pluginSequence);
  }

  // ======== plugin functions ========

  // eslint-disable-next-line class-methods-use-this
  public initialize(): Promise<void> {
    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
