import 'typeface-roboto';

import { StyledFlexibleLayout } from '@fki/editor-core/theme/StyledFlexibleLayout';
import { Box, CircularProgress, CssBaseline } from '@mui/material';
import React, { ReactElement, useEffect, useMemo, useState } from 'react';

import { RoutingPoint } from '@adlete/visualizer-core/RouteingPoint';
import { LoginLinkButton } from '@adlete/visualizer-core/routes/LoginLinkButton';

import { CustomThemeProvider } from '../plugins/theme/hooks';

import { AppCore } from './AppCore';
import { AppCoreContext } from './AppCoreContext';
import { AppMenu } from './Menu';

interface IAppUIProps {
  core: AppCore;
}

function AppUI({ core }: IAppUIProps): ReactElement {
  const layoutMan = core.plugins.get('layoutManager');
  return (
    <CustomThemeProvider>
      <CssBaseline />
        <Box display="flex" flexDirection="column" style={{ height: '100%' }}>
          <Box display="flex" flexGrow="1" style={{ position: 'relative' }}>
            <StyledFlexibleLayout layoutManager={layoutMan} />        
          </Box>
        <Box>
          <AppMenu menuActions={<LoginLinkButton style={{marginLeft: '2em', fontSize:16, textDecoration: 'none'}}/>}/>
        </Box>
      </Box>
    </CustomThemeProvider>
  );
}

export function App(): ReactElement {
  const [isInitialized, setIsInitialized] = useState(false);
  const core = useMemo(() => new AppCore(), []);

  useEffect(() => {
    async function inner() {
      await core.start();
      setIsInitialized(true);
    }
    inner();
  }, [core]);
  
  // only render application when project-manager was properly initialized
  let content = null;
  if (isInitialized) {
  content = <RoutingPoint mainPage={<AppUI core={core} />}></RoutingPoint>;
  } else {
    content = <CircularProgress />;
  }

  return <AppCoreContext.Provider value={core}>{content}</AppCoreContext.Provider>;
}
