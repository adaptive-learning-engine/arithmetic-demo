import React from 'react';

import type { AppCore } from './AppCore';

export const AppCoreContext = React.createContext<AppCore>(null);
