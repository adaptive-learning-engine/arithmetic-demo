#!/bin/bash
# installs and builds the service
cd /home/node/adlete-service

yarn build:node-agrum-linux64
yarn build:engine-blocks
yarn build:framework
yarn build:service

mkdir build
mkdir -p build/framework
cp -r adlete-packages/@adlete/framework/build build/framework/
cp adlete-packages/@adlete/framework/package.json build/framework/package.json

mkdir -p build/engine-blocks
cp -r adlete-packages/@adlete/engine-blocks/build build/engine-blocks/
cp adlete-packages/@adlete/engine-blocks/package.json build/engine-blocks/package.json

mkdir -p build/service
cp -r adlete-packages/@adlete/service/build build/service/
cp adlete-packages/@adlete/service/package.json build/service/package.json

mkdir -p build/node-agrum
cp -r adlete-packages/@adlete/node-agrum/build build/node-agrum/
cp adlete-packages/@adlete/node-agrum/package.json build/node-agrum/package.json
